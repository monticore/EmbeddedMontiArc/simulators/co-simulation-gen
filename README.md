<!-- (c) https://github.com/MontiCore/monticore -->
# Co-Simulation Generator
[![pipeline status](https://git.rwth-aachen.de/monticore/EmbeddedMontiArc/simulators/co-simulation-gen/badges/master/pipeline.svg)](https://git.rwth-aachen.de/monticore/EmbeddedMontiArc/simulators/co-simulation-gen/commits/master)

The Co-Simulation Generator project is responsible for the generation step of the co-simulation framework. 
It generates the Java classes `ConcreteExecution` and `StartExecution` which contain all information that are required to start the co-simulation.

There are two possibilities to use the co-simulation generator:
* [Co-Simulator Command-Line Interface](https://git.rwth-aachen.de/monticore/EmbeddedMontiArc/simulators/montisim-cli): Manual usage &rarr; Quick but only for experiments.
* [Co-Simulator Maven Plugin](https://git.rwth-aachen.de/monticore/EmbeddedMontiArc/simulators/maven-cosimulation-gen-plugin): Automatic execution &rarr; For simulation setups which configuration can change over time.

For instructions how to use the CLI and the Maven plugin respectively consider the corresponding Gitlab documentation. 
