/* (c) https://github.com/MontiCore/monticore */
/**
 * 
 */
package montisim;

import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.Test;

import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ExpandedComponentInstanceSymbol;

/**
 *
 */
public class FreemarkerUtilTest {

	@Test
	public void testConfigurationAndTemplates() {
		FreemarkerUtil freemarkerUtil = new FreemarkerUtil("target/generated-sources", "target/generated-sources");
		assertNotNull(freemarkerUtil);
	}
	
	@Test
	public void testProcessing() {
		FreemarkerUtil freemarkerUtil = new FreemarkerUtil("target/generated-sources", "target/generated-sources");
		assertNotNull(freemarkerUtil);
		
		EMAUtil emaUtil = new EMAUtil("src/test/resources", "setup.composition");
		assertNotNull(emaUtil);
		
		ExpandedComponentInstanceSymbol topComponent = emaUtil.getTopComponent();
		assertNotNull(topComponent);
		
		List<ExpandedComponentInstanceSymbol> executionOrder = emaUtil.findExecutionOrder();
		freemarkerUtil.processTemplates(topComponent, executionOrder);
	}
}
