/* (c) https://github.com/MontiCore/monticore */
/**
 * 
 */
package montisim;

import org.junit.Test;

/**
 *
 */
public class SimGeneratorTest {
	
	@Test
	public void testGenerator() {
		SimGenerator.generate("target/generated-sources", "target/generated-sources", "src/test/resources", "setup.loopComposition");
		SimGenerator.generate("target/generated-sources", "target/generated-sources", "src/test/resources", "setup.montiSimComposition");
		SimGenerator.generate("target/generated-sources", "target/generated-sources", "src/test/resources", "setup.composition");
	}
}
