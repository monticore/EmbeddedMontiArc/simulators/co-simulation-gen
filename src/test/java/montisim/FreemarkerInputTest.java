/* (c) https://github.com/MontiCore/monticore */
package montisim;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.junit.Test;

import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ExpandedComponentInstanceSymbol;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.PortSymbol;

public class FreemarkerInputTest {

	@Test
	public void testTransformation() {
		EMAUtil emaUtil = new EMAUtil("src/test/resources", "setup.composition");
		ExpandedComponentInstanceSymbol topComponent = emaUtil.getTopComponent();
		List<ExpandedComponentInstanceSymbol> simulatorList = emaUtil.findExecutionOrder();
		
		checkSim1(topComponent, simulatorList);
		checkSim2(topComponent, simulatorList);
		checkSim3(topComponent, simulatorList);
	}

	private void checkSim1(ExpandedComponentInstanceSymbol topComponent,
			List<ExpandedComponentInstanceSymbol> simulatorList) {
		
		FreemarkerInput freemarkerInput1 = new FreemarkerInput(simulatorList.get(0), topComponent);
		assertEquals("Sim1", freemarkerInput1.getClassName());
		assertEquals("sim1", freemarkerInput1.getObjectName());
		assertEquals("simulations.Sim1", freemarkerInput1.getPackageName());
		assertEquals(true, freemarkerInput1.hasInputPorts());
		assertEquals(true, freemarkerInput1.hasOutputPorts());
		
		assertEquals(1, freemarkerInput1.getInputPorts().size());
		ArrayList<ImmutablePair<PortSymbol, PortSymbol>> inputPorts = new ArrayList<>();
		for (ImmutablePair<PortSymbol, PortSymbol> immutablePair : freemarkerInput1.getInputPorts()) {
			inputPorts.add(immutablePair);
		}
		assertEquals("deltaTime", inputPorts.get(0).left.getName());
		assertEquals("in1", inputPorts.get(0).right.getName());
		
		assertEquals(1, freemarkerInput1.getOutputPorts().size());
		ArrayList<ImmutablePair<PortSymbol, PortSymbol>> outputPorts = new ArrayList<>();
		for (ImmutablePair<PortSymbol, PortSymbol> immutablePair : freemarkerInput1.getOutputPorts()) {
			outputPorts.add(immutablePair);
		}
		assertEquals("out1", outputPorts.get(0).left.getName());
		freemarkerInput1.toString();
	}
	
	private void checkSim2(ExpandedComponentInstanceSymbol topComponent,
			List<ExpandedComponentInstanceSymbol> simulatorList) {
		
		FreemarkerInput freemarkerInput1 = new FreemarkerInput(simulatorList.get(1), topComponent);
		assertEquals("Sim2", freemarkerInput1.getClassName());
		assertEquals("sim2", freemarkerInput1.getObjectName());
		assertEquals("simulations.Sim2", freemarkerInput1.getPackageName());
		assertEquals(true, freemarkerInput1.hasInputPorts());
		assertEquals(true, freemarkerInput1.hasOutputPorts());
		
		assertEquals(1, freemarkerInput1.getInputPorts().size());
		ArrayList<ImmutablePair<PortSymbol, PortSymbol>> inputPorts = new ArrayList<>();
		for (ImmutablePair<PortSymbol, PortSymbol> immutablePair : freemarkerInput1.getInputPorts()) {
			inputPorts.add(immutablePair);
		}
		assertEquals("deltaTime", inputPorts.get(0).left.getName());
		assertEquals("in1", inputPorts.get(0).right.getName());
		
		assertEquals(1, freemarkerInput1.getOutputPorts().size());
		ArrayList<ImmutablePair<PortSymbol, PortSymbol>> outputPorts = new ArrayList<>();
		for (ImmutablePair<PortSymbol, PortSymbol> immutablePair : freemarkerInput1.getOutputPorts()) {
			outputPorts.add(immutablePair);
		}
		assertEquals("out1", outputPorts.get(0).left.getName());
		freemarkerInput1.toString();
	}
	
	private void checkSim3(ExpandedComponentInstanceSymbol topComponent,
			List<ExpandedComponentInstanceSymbol> simulatorList) {
		
		FreemarkerInput freemarkerInput1 = new FreemarkerInput(simulatorList.get(2), topComponent);
		assertEquals("Sim3", freemarkerInput1.getClassName());
		assertEquals("sim3", freemarkerInput1.getObjectName());
		assertEquals("simulations.Sim3", freemarkerInput1.getPackageName());
		assertEquals(true, freemarkerInput1.hasInputPorts());
		assertEquals(true, freemarkerInput1.hasOutputPorts());
		
		assertEquals(3, freemarkerInput1.getInputPorts().size());
		ArrayList<ImmutablePair<PortSymbol, PortSymbol>> inputPorts = new ArrayList<>();
		for (ImmutablePair<PortSymbol, PortSymbol> immutablePair : freemarkerInput1.getInputPorts()) {
			inputPorts.add(immutablePair);
		}
		assertEquals("deltaTime", inputPorts.get(0).left.getName());
		assertEquals("in1", inputPorts.get(0).right.getName());
		assertEquals("in1", inputPorts.get(1).left.getName());
		assertEquals("out1", inputPorts.get(1).right.getName());
		assertEquals("in2", inputPorts.get(2).left.getName());
		assertEquals("out1", inputPorts.get(2).right.getName());
		
		assertEquals(1, freemarkerInput1.getOutputPorts().size());
		ArrayList<ImmutablePair<PortSymbol, PortSymbol>> outputPorts = new ArrayList<>();
		for (ImmutablePair<PortSymbol, PortSymbol> immutablePair : freemarkerInput1.getOutputPorts()) {
			outputPorts.add(immutablePair);
		}
		assertEquals("out1", outputPorts.get(0).left.getName());
		freemarkerInput1.toString();
		
		freemarkerInput1.addInitValue("in1", 10);
		freemarkerInput1.addInitValue("in1", 30);
		assertEquals(10, freemarkerInput1.getInitValueForPort("in1"), 0);
	}
}
