/* (c) https://github.com/MontiCore/monticore */
package montisim;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.Test;

import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ExpandedComponentInstanceSymbol;

public class EMAUtilTest {

	@Test
	public void testParsing() {
		EMAUtil emaUtil = new EMAUtil("src/test/resources", "setup.composition");
		assertNotNull(emaUtil);
		
		ExpandedComponentInstanceSymbol topComponent = emaUtil.getTopComponent();
		assertNotNull(topComponent);
		assertEquals("Composition", topComponent.getComponentType().getName());
		assertEquals("composition", topComponent.getName());
		
		List<ExpandedComponentInstanceSymbol> executionOrder = emaUtil.findExecutionOrder();
		assertEquals(3, executionOrder.size());
		assertEquals("sim1", executionOrder.get(0).getName());
		assertEquals("sim2", executionOrder.get(1).getName());
		assertEquals("sim3", executionOrder.get(2).getName());
	}
	
	@Test
	public void testLoopParsing() {
		EMAUtil emaUtil = new EMAUtil("src/test/resources", "setup.loopComposition");
		assertNotNull(emaUtil);
		
		ExpandedComponentInstanceSymbol topComponent = emaUtil.getTopComponent();
		assertNotNull(topComponent);
		assertEquals("LoopComposition", topComponent.getComponentType().getName());
		assertEquals("loopComposition", topComponent.getName());
		
		List<ExpandedComponentInstanceSymbol> executionOrder = emaUtil.findExecutionOrder();
		assertEquals(3, executionOrder.size());
		assertEquals("simLoop", executionOrder.get(0).getName());
		assertEquals("sim4", executionOrder.get(1).getName());
		assertEquals("sim5", executionOrder.get(2).getName());
	}
	
	@Test
	public void testMontiSimParsing() {
		EMAUtil emaUtil = new EMAUtil("src/test/resources", "setup.montiSimComposition");
		assertNotNull(emaUtil);
		
		ExpandedComponentInstanceSymbol topComponent = emaUtil.getTopComponent();
		assertNotNull(topComponent);
		assertEquals("MontiSimComposition", topComponent.getComponentType().getName());
		assertEquals("montiSimComposition", topComponent.getName());
		
		List<ExpandedComponentInstanceSymbol> executionOrder = emaUtil.findExecutionOrder();
		assertEquals(1, executionOrder.size());
		assertEquals("simulator", executionOrder.get(0).getName());
	}
}
