<#-- (c) https://github.com/MontiCore/monticore -->
import montisim.SimExecutor;

public class ExecutionStart {

	public static void main(String[] args) {
		if (args.length != 2) {
			System.err.println("Invalid arguments! 1.End Time 2.Loop Frequency");
			System.exit(1);
		}
		SimExecutor simExecutor = new SimExecutor(Double.parseDouble(args[0]), Double.parseDouble(args[1]));
		ConcreteExecution execution = new ConcreteExecution();
		
		simExecutor.setExecution(execution);
		execution.initInputPorts();
		<#assign isMontiSimIn = false>
		<#list simulators as simulator>
		<#if simulator.getClassName() == "Simulator">
		<#assign isMontiSimIn = true >
		</#if>
		</#list>
		<#if isMontiSimIn> 
		execution.initMainSimulator();
		simExecutor.execute();
		execution.stopMainSimulator();
		<#else>
		simExecutor.execute();
		</#if>
	}
}
