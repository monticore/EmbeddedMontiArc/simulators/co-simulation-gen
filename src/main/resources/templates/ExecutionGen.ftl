<#-- (c) https://github.com/MontiCore/monticore -->
<#ftl strip_whitespace = true>
<#setting locale="en_US">
<#-- IMPORT ALL DEPENDENCIES -->
<#list simulators as simulator>
<#if simulator.isMontiSim()>
import java.util.Optional;
import commons.controller.interfaces.Bus;
import databus.DataBus;
import mainControlBlock.MainControlBlock;
import montisim.Execution;
import navigationBlock.NavigationBlock;
import sensors.util.SensorUtil;
import simulation.environment.WorldModel;
import simulation.environment.osm.ParserSettings;
import simulation.environment.visualisationadapter.implementation.Node2D;
import simulation.environment.weather.Weather;
import simulation.environment.weather.WeatherSettings;
import simulation.network.NetworkCellBaseStation;
import simulation.network.NetworkSimulator;
import simulation.simulator.SimulationPlotter2D;
import simulation.simulator.SimulationType;
import simulation.simulator.Simulator;
import simulation.vehicle.PhysicalVehicle;
import simulation.vehicle.PhysicalVehicleBuilder;
import simulation.vehicle.RandomStatusLogger;
<#else>
import ${simulator.getPackageName()};
</#if>
</#list>
import montisim.Execution;

public class ConcreteExecution implements Execution {
	
	private double curTime = 0;
	
	<#-- INSTANCIATE THE CO-SIMULATORS -->
	<#list simulators as simulator>
	<#if simulator.isMontiSim()>
	Simulator ${simulator.getObjectName()} = Simulator.getSharedInstance();
	<#else>
	${simulator.getClassName()} ${simulator.getObjectName()} = new ${simulator.getClassName()}();
	</#if>
	</#list>

	@Override
	public void executeAll(double deltaTime) {

		curTime += deltaTime;

		<#list simulators as simulator>
		<#-- SET INPUT PORTS -->
		<#if simulator.hasInputPorts()>
		<#list simulator.getInputPorts() as input>
		<#assign inputPortName = input.getKey().getName()>
		<#if inputPortName == "deltaTime">
		${simulator.getObjectName()}.setDeltaTime(deltaTime);
		<#elseif input.getValue()??>
		<#assign sourceComponentName = input.getValue().getComponentInstance().get().getName()>
		<#assign sourcePortName = input.getValue().getName()>
		<#assign objectName = simulator.getObjectName()>
		<#if inputPortName?contains("vehicle") && sourcePortName?contains("vehicle")>
		${objectName}.set${inputPortName[0..6] + inputPortName[8..]}(${inputPortName[7]}, ${sourcePortName}.get${sourcePortName[0..6] + sourcePortName[8..]}(sourcePortName[7]));
		<#elseif inputPortName?contains("vehicle")>
		${objectName}.set${inputPortName[0..6] + inputPortName[8..]}(${inputPortName[7]}, ${sourceComponentName}.get${sourcePortName}());
		<#elseif sourcePortName?contains("vehicle")>
		${objectName}.set${inputPortName}(${sourceComponentName}.get${sourcePortName[0..6] + sourcePortName[8..]}(${sourcePortName[7]}));
		<#else>
		${objectName}.set${inputPortName}(${sourceComponentName}.get${sourcePortName}());
		</#if>
		</#if>
		</#list>
		</#if>
		</#list>

		<#list simulators as simulator>
		${simulator.getObjectName()}.execute();
		</#list>
	}

	public void initInputPorts() {
		<#list simulators as simulator>
		<#if simulator.hasInputPorts()>
		<#list simulator.getInputPorts() as input>
		<#assign inputPortName = input.getKey().getName()>
		<#assign objectName = simulator.getObjectName()>
		<#if simulator.hasInitialValue(inputPortName)>
		<#assign sourceComponentName = input.getValue().getComponentInstance().get().getName()>
		<#assign sourcePortName = input.getValue().getName()>
		<#if sourcePortName?contains("vehicle")>
		${sourceComponentName}.set${sourcePortName[0..6] + sourcePortName[8..]}(${sourcePortName[7]}, ${simulator.getInitValueForPort(inputPortName)});
		<#else>
		${sourceComponentName}.set${sourcePortName}(${simulator.getInitValueForPort(inputPortName)});
		</#if>
		</#if>
		</#list>
		</#if>
		</#list>
	}

	private double getCurrentTime() {
	    return curTime;
    }

	@Override
	public String getCurOutput() {
		StringBuilder stringBuilder = new StringBuilder();
		<#list topComponent.getOutputPorts() as output>
		stringBuilder.append("${output.getKey().getName()}: ");
		<#assign sourcePortName = output.getValue().getName()>
		<#if sourcePortName?contains("vehicle")>
		stringBuilder.append(${output.getValue().getComponentInstance().get().getName()}.get${sourcePortName[0..6] + sourcePortName[8..]}(${sourcePortName[7]}) + "; ");
		<#else>
		stringBuilder.append(${output.getValue().getComponentInstance().get().getName()}.get${sourcePortName}() + "; ");
		</#if>
		</#list>
		return stringBuilder.toString();
	}

	@Override
	public String getSimulationData() {
		StringBuilder stringBuilder = new StringBuilder();
		<#list topComponent.getOutputPorts() as output>
		<#assign sourcePortName = output.getValue().getName()>
		<#if sourcePortName?contains("vehicle")>
		stringBuilder.append(${output.getValue().getComponentInstance().get().getName()}.get${sourcePortName[0..6] + sourcePortName[8..]}(${sourcePortName[7]}) + "; ");
		<#else>
		stringBuilder.append(${output.getValue().getComponentInstance().get().getName()}.get${sourcePortName}() + "; ");
		</#if>
		</#list>
		return stringBuilder.toString();
	}

	@Override
	public String getOutputNames() {
		StringBuilder stringBuilder = new StringBuilder();
		<#list topComponent.getOutputPorts() as output>
		<#assign sourcePortName = output.getValue().getName()>
		stringBuilder.append("${sourcePortName}" + ";");
		</#list>
		return stringBuilder.toString();
	}

	@Override
	public boolean checkFlags() {
		boolean flag = false;

		<#list simulators as simulator>
		flag = flag || ${simulator.getObjectName()}.getFlag();
		</#list>

		return flag;
	}

	@Override
	public void saveAllStates() {
		<#list simulators as simulator>
		${simulator.getObjectName()}.saveState();
		</#list>
	}

	@Override
	public void loadAllStates() {
		<#list simulators as simulator>
		${simulator.getObjectName()}.loadState();
		</#list>
	}

	<#list simulators as simulator>
	<#if simulator.isMontiSim()>

	public void initMainSimulator() {        
        // Simulation setup with default values
        ${simulator.getObjectName()}.setSimulationType(SimulationType.CO_SIMULATION);
        ${simulator.getObjectName()}.setSynchronousSimulation(true);
        addWorld();
        addPlotter();
        addNetworkSimulation();        
        addVehicles();
        ${simulator.getObjectName()}.prepareExecution();
	}

	private void addWorld() {
		boolean withHeightMap = false;
		WeatherSettings weatherSettings = new WeatherSettings(Weather.SUNSHINE);
        
        ParserSettings parserSettings = null;

        if (withHeightMap) {
            parserSettings = new ParserSettings("/map_ahornstrasse.osm", ParserSettings.ZCoordinates.STATIC);
        } else {
            parserSettings = new ParserSettings("/map_ahornstrasse.osm", ParserSettings.ZCoordinates.ALLZERO);
        }

        try {
            WorldModel.init(parserSettings, weatherSettings);
        } catch (Exception e) {
            e.printStackTrace();
        }
	}

	private void addPlotter() {
		SimulationPlotter2D plotter2D = new SimulationPlotter2D();
        ${simulator.getObjectName()}.registerLoopObserver(plotter2D);
	}

	private void addNetworkSimulation() {
		NetworkSimulator.resetInstance();
        NetworkSimulator networkSimulator = NetworkSimulator.getInstance();
        ${simulator.getObjectName()}.registerLoopObserver(networkSimulator);
        NetworkCellBaseStation baseStation1 = new NetworkCellBaseStation();
        ${simulator.getObjectName()}.registerAndPutObject(baseStation1, 800.0, 750.0, 0.0);
        NetworkCellBaseStation baseStation2 = new NetworkCellBaseStation();
        ${simulator.getObjectName()}.registerAndPutObject(baseStation2, 1300.0, 750.0, 0.0);
        NetworkCellBaseStation baseStation3 = new NetworkCellBaseStation();
        ${simulator.getObjectName()}.registerAndPutObject(baseStation3, 1800.0, 750.0, 0.0);
        baseStation2.addConnectedBaseStationID(baseStation1.getId());
        baseStation2.addConnectedBaseStationID(baseStation3.getId());
        baseStation1.addConnectedBaseStationID(baseStation2.getId());
        baseStation3.addConnectedBaseStationID(baseStation2.getId());
	}

	private void addVehicles() {
		PhysicalVehicle physicalVehicle = null;
        physicalVehicle = setupDefaultVehicle();
        ${simulator.getObjectName()}.registerAndPutObject(physicalVehicle, 1584.31626412008, 877.404690000371, 0.5 * Math.PI);
        physicalVehicle.getSimulationVehicle().navigateTo(new Node2D(1593.322563254177, 591.3829242001566, 0.0, 1223037297));
        physicalVehicle = setupDefaultVehicle();
        ${simulator.getObjectName()}.registerAndPutObject(physicalVehicle, 1474.5398601532963, 837.4404676004327, 1.4 * Math.PI);
        physicalVehicle.getSimulationVehicle().navigateTo(new Node2D(1584.3162641200822, 877.404690000371, 0.0, 205455272));
	}
	
    private static PhysicalVehicle setupDefaultVehicle() {
        // Get builder for physical vehicles
        PhysicalVehicleBuilder physicalVehicleBuilder = PhysicalVehicleBuilder.getInstance();

        // Build vehicle with controller
        Bus bus = new DataBus();
        MainControlBlock mainControlBlock = new MainControlBlock();
        NavigationBlock navigationBlock = new NavigationBlock();

        PhysicalVehicle physicalVehicle = physicalVehicleBuilder.buildPhysicalVehicle(Optional.of(bus), Optional.of(mainControlBlock), Optional.of(navigationBlock));
        SensorUtil.sensorAdder(physicalVehicle);
        physicalVehicle.getSimulationVehicle().setStatusLogger(new RandomStatusLogger());
        return physicalVehicle;
    }

	public void stopMainSimulator() {
		${simulator.getObjectName()}.stopSimulation();
	}
	</#if>
	</#list>
}
