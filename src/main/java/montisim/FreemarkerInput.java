/* (c) https://github.com/MontiCore/monticore */
/**
 * 
 */
package montisim;

import java.util.List;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.lang3.tuple.ImmutablePair;

import de.monticore.expressionsbasis._ast.ASTExpression;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ConnectorSymbol;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ExpandedComponentInstanceSymbol;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.PortSymbol;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarc.types.EMAVariable;
import de.monticore.lang.math._symboltable.expression.MathNumberExpressionSymbol;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * The FreemarkerInput class is a wrapper class for the Freemarker template. An
 * instance of the FreemarkerInput represents a component in the ema file and
 * thus a single simulator. It contains the preprocessed information which are
 * necessary for the generating process.
 * 
 *
 */
public class FreemarkerInput {

    private static final Logger LOGGER = LogManager.getLogger(FreemarkerInput.class);

    // Name of the component defined in ema file, determines the class at runtime
    private final String className;

    // Name of the concrete object which is defined in the ema file
    private final String objectName;

    // Name of the package which the co-simulator belongs to
    private final String packageName;

    private final boolean hasInputPorts;
    // Input ports with their corresponding source: <InputPort, connectedOutputPort>
    private final Collection<ImmutablePair<PortSymbol, PortSymbol>> inputPorts = new ArrayList<ImmutablePair<PortSymbol, PortSymbol>>();

    private final boolean hasOutputPorts;
    // Output ports with their corresponding target: <OutputPort,
    // connectedInputPort>
    // The connectedInputPort is only determined for the top component!
    // For inner components only the outputPort is included.
    private final Collection<ImmutablePair<PortSymbol, PortSymbol>> outputPorts = new ArrayList<ImmutablePair<PortSymbol, PortSymbol>>();

    // Initial values for input ports provided in the ema configuration
    private final Map<String, Double> initialValues = new HashMap<String, Double>();

    public FreemarkerInput(ExpandedComponentInstanceSymbol expandedComponentInstanceSymbol,
            ExpandedComponentInstanceSymbol topComponent) {
        LOGGER.debug("Create new FreeMarkerInput instance of co-simulator.");
        this.className = expandedComponentInstanceSymbol.getComponentType().getName();
        this.objectName = expandedComponentInstanceSymbol.getName();

        String fullName = expandedComponentInstanceSymbol.getComponentType().getFullName();
        this.packageName = fullName.substring(fullName.indexOf(".") + 1);

        addInputPorts(expandedComponentInstanceSymbol, topComponent);
        this.hasInputPorts = !expandedComponentInstanceSymbol.getIncomingPorts().isEmpty();

        addOutputPorts(expandedComponentInstanceSymbol, topComponent);
        this.hasOutputPorts = !expandedComponentInstanceSymbol.getOutgoingPorts().isEmpty();

        fillInitialParameters(expandedComponentInstanceSymbol);
    }

    private void fillInitialParameters(ExpandedComponentInstanceSymbol expandedComponentInstanceSymbol) {
        List<EMAVariable> parameters = expandedComponentInstanceSymbol.getParameters();
        List<ASTExpression> arguments = expandedComponentInstanceSymbol.getArguments();

        if (parameters.size() != arguments.size()) {
            LOGGER.error(String.format("Number of parameters(%s) and arguments(%s) differ!", parameters.size(),
                    arguments.size()));
        }

        if (!parameters.isEmpty()) {
            LOGGER.info(String.format("Use initial values of component %s.", objectName));
        }

        for (int i = 0; i < parameters.size(); i++) {
            String portName = parameters.get(i).getName();
            MathNumberExpressionSymbol argument = (MathNumberExpressionSymbol) arguments.get(i).getSymbolOpt().get();
            double portValue = Double.parseDouble(argument.getTextualRepresentation());
            initialValues.put(portName, portValue);
        }
    }

    private void addOutputPorts(ExpandedComponentInstanceSymbol expandedComponentInstanceSymbol,
            ExpandedComponentInstanceSymbol topComponent) {
        LOGGER.debug(String.format("Find output ports of component %s.", objectName));

        PortSymbol sourcePort = null;
        for (PortSymbol outputPort : expandedComponentInstanceSymbol.getOutgoingPorts()) {
            for (Iterator<ConnectorSymbol> iterator = topComponent.getConnectors().iterator(); iterator.hasNext();) {
                ConnectorSymbol connector = (ConnectorSymbol) iterator.next();
                if (connector.getTargetPort().equals(outputPort)) {
                    if (sourcePort != null) {
                        LOGGER.error("Invalid ema file: Multiple inputs to port " + outputPort.getName()
                                + " of simulator " + outputPort.getComponentInstance().get().getName());
                    } else {
                        sourcePort = connector.getSourcePort();
                    }
                }
            }
            if (sourcePort == null && expandedComponentInstanceSymbol.equals(topComponent)) {
                LOGGER.fatal("Top component output port " + outputPort.getName() + " has no source!",
                        new Exception("Invalid ema configuration"));
            }
            this.outputPorts.add(new ImmutablePair<PortSymbol, PortSymbol>(outputPort, sourcePort));
            sourcePort = null;
        }
    }

    private void addInputPorts(ExpandedComponentInstanceSymbol expandedComponentInstanceSymbol,
            ExpandedComponentInstanceSymbol topComponent) {
        LOGGER.debug(String.format("Find input ports of component %s.", objectName));

        for (PortSymbol inputPort : expandedComponentInstanceSymbol.getIncomingPorts()) {
            PortSymbol sourcePort = getSource(inputPort, topComponent);
            this.inputPorts.add(new ImmutablePair<PortSymbol, PortSymbol>(inputPort, sourcePort));
        }
    }

    private PortSymbol getSource(PortSymbol inputPort, ExpandedComponentInstanceSymbol topComponent) {
        PortSymbol sourcePort = null;
        if (inputPort.getTargetConnectedPorts(topComponent).isEmpty()) {
            LOGGER.warn("Simulator " + inputPort.getComponentInstance().get().getName() + " has port "
                    + inputPort.getName() + " with no input.");
        } else if (inputPort.getTargetConnectedPorts(topComponent).size() > 1) {
            LOGGER.error(
                    "The simulation setup is invalid. Simulator " + inputPort.getComponentInstance().get().getName()
                            + " has port " + inputPort.getName() + " with multiple inputs.",
                    new Exception("Invalid ema configuration"));
        } else {
            sourcePort = inputPort.getTargetConnectedPorts(topComponent).get(0);
        }
        return sourcePort;
    }

    public String getClassName() {
        return className;
    }

    public String getObjectName() {
        return objectName;
    }

    public String getPackageName() {
        return packageName;
    }

    public boolean hasInputPorts() {
        return hasInputPorts;
    }

    public Collection<ImmutablePair<PortSymbol, PortSymbol>> getInputPorts() {
        return inputPorts;
    }

    public boolean hasOutputPorts() {
        return hasOutputPorts;
    }

    public Collection<ImmutablePair<PortSymbol, PortSymbol>> getOutputPorts() {
        return outputPorts;
    }

    public String toString() {
        return String.format("Class: %s; Name: %s", getClassName(), getObjectName());
    }

    public void addInitValue(String inputPortName, double value) {
        if (!initialValues.containsKey(inputPortName)) {
            initialValues.put(inputPortName, value);
        } else {
            LOGGER.warn(String.format("Port %s already has an initial value.", inputPortName));
        }
    }

    public double getInitValueForPort(String inputPortName) {
        double initialValue = 0;
        if (initialValues.containsKey(inputPortName)) {
            initialValue = initialValues.get(inputPortName);
        } else {
            LOGGER.error(String.format("Port %s has no initial value.", inputPortName));
        }
        return initialValue;
    }

    public boolean hasInitialValue(String portName) {
        return initialValues.containsKey(portName);
    }

    public boolean isMontiSim() {
        return "Simulator".equals(getClassName());
    }
}
