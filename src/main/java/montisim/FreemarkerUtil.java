/* (c) https://github.com/MontiCore/monticore */
/**
 * 
 */
package montisim;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ExpandedComponentInstanceSymbol;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 */
public class FreemarkerUtil {

    private static final Logger LOGGER = LogManager.getLogger(FreemarkerUtil.class);

    // Constants
    private static final String ENCODING = "UTF-8";
    private static final String TEMPLATE_GEN_NAME = "ExecutionGen.ftl";
    private static final String TEMPLATE_START_NAME = "ExecutionStart.ftl";
    private static final String TEMPLATE_SIMULATORS_KEY = "simulators";
    private static final String TEMPLATE_TOPCOMPONENT_KEY = "topComponent";
    private static final String TEMPLATE_EROR = "Could not load template";

    private String targetDirExecution;
    private String targetDirStart;

    private Configuration configuration;

    public FreemarkerUtil(String targetDirExecution, String targetDirStart) {
        LOGGER.debug("Create FreeMarkerUtil instance.");
        this.targetDirExecution = targetDirExecution + "/ConcreteExecution.java";
        this.targetDirStart = targetDirStart + "/ExecutionStart.java";
        createFreemarkerConfig();
    }

    private void createFreemarkerConfig() {
        LOGGER.debug("Create FreeMarker configuration.");
        configuration = new Configuration(Configuration.VERSION_2_3_28);
        configuration.setClassForTemplateLoading(FreemarkerUtil.class, "/templates");
        configuration.setDefaultEncoding(ENCODING);
        configuration.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
    }

    public void processTemplates(ExpandedComponentInstanceSymbol topComponent,
            List<ExpandedComponentInstanceSymbol> components) {
        // Key must be the name which is used in the template
        Map<String, Object> executionInput = new HashMap<String, Object>();
        Map<String, Object> startInput = new HashMap<String, Object>();
        prepareInput(topComponent, components, executionInput, startInput);

        Template executionTemplate = null;
        Template startTemplate = null;
        try {
            LOGGER.info("Load Freemarker execution template");
            executionTemplate = configuration.getTemplate(TEMPLATE_GEN_NAME);
            LOGGER.info("Load Freemarker start template");
            startTemplate = configuration.getTemplate(TEMPLATE_START_NAME);
        } catch (IOException e) {
            LOGGER.fatal(TEMPLATE_EROR, e);
        }

        if (executionTemplate == null) {
            LOGGER.fatal("Could not load execution template", new Exception("Template error"));
        }
        if (startTemplate == null) {
            LOGGER.fatal("Could not load start template", new Exception("Template error"));
        }
        generateClasses(executionInput, startInput, executionTemplate, startTemplate);
    }

    private void generateClasses(Map<String, Object> executionInput, Map<String, Object> startInput,
            Template executionTemplate, Template startTemplate) {
        try (Writer executionFileWriter = new BufferedWriter(
                new OutputStreamWriter(new FileOutputStream(targetDirExecution), ENCODING));
                Writer startfileWriter = new BufferedWriter(
                        new OutputStreamWriter(new FileOutputStream(targetDirStart), ENCODING))) {
            LOGGER.info("Process Freemarker execution template");
            executionTemplate.process(executionInput, executionFileWriter);
            LOGGER.info("Process Freemarker start template");
            startTemplate.process(startInput, startfileWriter);
        } catch (FileNotFoundException e) {
            LOGGER.error("Could not create Writer", e);
        } catch (IOException e) {
            LOGGER.error("Could not create Writer", e);
        } catch (TemplateException e) {
            LOGGER.error("Could not process Template", e);
        }
    }

    private void prepareInput(ExpandedComponentInstanceSymbol topComponent,
            List<ExpandedComponentInstanceSymbol> components, Map<String, Object> executionInput,
            Map<String, Object> startInput) {
        LOGGER.info("Preparing FreeMarker data model.");

        List<FreemarkerInput> simulators = new ArrayList<FreemarkerInput>();
        for (ExpandedComponentInstanceSymbol expandedComponentInstanceSymbol : components) {
            simulators.add(new FreemarkerInput(expandedComponentInstanceSymbol, topComponent));
        }
        FreemarkerInput expandedTopComponent = new FreemarkerInput(topComponent, topComponent);

        executionInput.put(TEMPLATE_SIMULATORS_KEY, simulators);
        executionInput.put(TEMPLATE_TOPCOMPONENT_KEY, expandedTopComponent);
        startInput.put(TEMPLATE_SIMULATORS_KEY, simulators);
    }
}
