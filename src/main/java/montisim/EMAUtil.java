/* (c) https://github.com/MontiCore/monticore */
package montisim;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ExpandedComponentInstanceSymbol;
import de.monticore.lang.monticar.generator.order.ImplementExecutionOrder;
import de.monticore.lang.monticar.generator.order.simulator.AbstractSymtab;
import de.monticore.lang.tagging._symboltable.TaggingResolver;

public class EMAUtil {

    private static final Logger LOGGER = LogManager.getLogger(EMAUtil.class);

    private TaggingResolver symTab;
    private ExpandedComponentInstanceSymbol topComponent;

    private String pathToResolve;
    private String fileName;

    public EMAUtil(String pathToResolve, String fileName) {
        this.pathToResolve = pathToResolve;
        this.fileName = fileName;
        readEMA();
    }

    public ExpandedComponentInstanceSymbol getTopComponent() {
        return topComponent;
    }

    private void readEMA() {
        LOGGER.info("Read EMAM files");

        symTab = AbstractSymtab.createSymTabAndTaggingResolver(pathToResolve);
        topComponent = symTab.<ExpandedComponentInstanceSymbol>resolve(fileName, ExpandedComponentInstanceSymbol.KIND)
                .orElse(null);

        if (symTab == null || topComponent == null) {
            LOGGER.fatal("Error when parsing EMA files.");
        }
    }

    public List<ExpandedComponentInstanceSymbol> findExecutionOrder() {
        LOGGER.info("Find execution order.");
        return ImplementExecutionOrder.exOrder(symTab, topComponent);
    }
}
