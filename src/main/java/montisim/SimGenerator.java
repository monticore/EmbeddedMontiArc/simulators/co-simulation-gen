/* (c) https://github.com/MontiCore/monticore */

package montisim;

import java.util.List;

import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ExpandedComponentInstanceSymbol;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 */
public class SimGenerator {

    private static final Logger LOGGER = LogManager.getLogger(SimGenerator.class);

    private SimGenerator() {}

    public static void generate(String targetDirExecution, String targetDirStart, String resolvePath,
            String emaFileName) {
        LOGGER.info("Start generation process.");

        FreemarkerUtil freemarkerUtil = new FreemarkerUtil(targetDirExecution, targetDirStart);
        EMAUtil emaUtil = new EMAUtil(resolvePath, emaFileName);
        List<ExpandedComponentInstanceSymbol> components = emaUtil.findExecutionOrder();
        ExpandedComponentInstanceSymbol topComponent = emaUtil.getTopComponent();
        freemarkerUtil.processTemplates(topComponent, components);

        LOGGER.info("Generation process finsihed.");
    }
}
